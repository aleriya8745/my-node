/**
 * Created by dunice on 24.03.17.
 */
var mongoose = require('mongoose');
var todoSchema = mongoose.Schema({
    text: String,
    checked: Boolean,
});


var todo = mongoose.model('todo', todoSchema);
module.exports = todo;