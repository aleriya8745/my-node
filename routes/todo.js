var express = require('express');
var router = express.Router();
var Todo = require('../db/models/todo');

router.get('/', function (req, res, next) {
//     var todoSchema = mongoose.Schema({
//         text: String,
//         checked: Boolean,
//     });
    Todo.find({}, function (err, todos) {
        if (err) res.send({error: true, errorMessage: "Sorry. We have some problems"});
        res.send(todos);
    });

});

router.post('/', function (req, res, next) {
    console.log(req.body.text);

    if (text = "") res.send({error: true, errorMessage: "Sorry. The field is empty"});


    Todo.create({text: req.body.text, checked: ''}, function (err, text) {
        if (err) res.send({error: true, errorMessage: "Sorry. We have some problems"});
        res.send(text);
    });
});

router.delete('/:id', function (req, res) {
    Todo.remove({_id: req.params.id}, function (err) {
        if (err) return res.send({error: true, errorMessage: "Sorry. The field is empty"});
        res.send(req.params.id);
    });
});

router.delete('/', function (req, res) {
    Todo.remove({checked: true}, function (err) {
        if (err) return res.send({error: true, errorMessage: "Sorry. The field is empty"});
        res.sendStatus(200);
    });
});

router.put('/:id', function (req, res) {
    console.log('isChecked: ', req.body.checked);
    Todo.update({_id: req.body._id}, req.body, {multi: true}, function (err) {
        if (err) return res.send({error: true, errorMessage: "Sorry. The field is empty"});
        res.send({change: true});
    });
});

router.put('/', function (req, res) {
    console.log('check all');
    let checked = JSON.parse(req.body.checked);
    Todo.update({checked: !checked}, {checked: checked}, {multi: true}, function (err) {
        if (err) return res.send({error: true, errorMessage: "Sorry. The field is empty"});
        res.send({change: true});

    });
});

module.exports = router;
