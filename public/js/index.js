let todoArr = [];
$(function () {


    const todoList = $('#todo-list'),
        todoInput = $('#todo'),
        liPerPage = 5;
    let allTodo = todoArr.length,
        arr = [],
        ifNotTodo = $("#ifNotTodo"),
        arrForPage,
        newVar;

    addSaveTodo();

    function addSaveTodo() {
        $.ajax({
            dataType: 'JSON',
            url: 'todo/',
            type: 'GET',
            success: function (data) {
                $('#todo-list li').remove();
                console.log('success');
                todoArr = data;
                findNumber();
            },
            error: (function () {
                console.log('error');
            })
        })
    }


    $('#todo2').click(function () {
        enter();
    });

    todoInput.keydown(event => {
        if (event.keyCode == 13) {
            enter();
        }
    });

    function enter() {
        const val = todoInput.val().trim();
        if (val) {
            console.log('function start');
            addNewTodo(val);
        }
    }

    function addNewTodo(val) {
        let ass= '';
        $.ajax({
            type: 'POST',
            url: 'todo/',
            dataType: "text",
            data: {text: val},
            success: function (result) {
                const data = JSON.parse(result);
                if (data.error) {
                    return alert(data.errorMessage);
                }
                console.log('success');
                ass += '<li id="' + data._id + '"><input type="checkbox" class="checkbox"/><label class="hidetext" for=".checkbox">' + data.text + '</label><input class="text" style="display: none" value="' + data.text + '"><input type="submit" class="delete" value="Delete"/></li>';
                todoInput.val("");
                todoArr.push(data);
                addSaveTodo();
                findNumber(true);
            }
        });
        todoList.html(ass);
    }


    $("body").on("change", '.checkbox', function () {
        console.log('change', this.checked);
        changeTodo($(this).parent().attr('id'), this.checked);

    });

    function changeTodo(Id, checked) {
        todoArr.forEach(value => {
            if (value._id == Id) {
                newVar = value;
                newVar.checked = checked;
                changeCheckTodo(newVar);
            }
        })
    }

    function changeCheckTodo(newVar) {
        $.ajax({
            type: 'PUT',
            url: 'todo/' + newVar,
            dataType: "JSON",
            data: newVar,
            success: function (result) {
                console.log(result);
                addSaveTodo();
            }
        })
    }


    $("body").on("click", ".delete", function () {
        console.log('change', this.checked);

        removeTodo($(this).parent().attr('id'));
    });
    function removeTodo(value) {
        $.ajax({
            url: 'todo/' + value,
            type: 'DELETE',
            success: function () {
                console.log('success');
                addSaveTodo();
            },
            error: function () {
                console.log('error');
            }
        });
    }

    $("body").on("change", '#allchecked', function () {
        console.log('change', this.checked);
        changeAllChecked(this.checked);
    });

    function changeAllChecked(сheck) {
            $.ajax({
                type: 'PUT',
                url: 'todo/',
                dataType: "JSON",
                data: {checked:сheck},
                success: function () {
                    console.log('it"s ok!!');
                    addSaveTodo();
                }
            });
    }

    $('#alldeleted').click(function () {
        removeAllComplete();
    });
    function removeAllComplete() {
        $.ajax({
            url: 'todo/',
            type: 'DELETE',
            success: function () {
                console.log('delete_all');
                $("#allchecked").prop('checked', false);
                addSaveTodo();
                findNumber();
            },
            error: function () {
                console.log('error');
            }
        });
    }

    $("body").on("dblclick", ".hidetext", function () {
        const   that = $(this),
                 varId = $(this).parent().attr('id');
        console.log($(this).attr('id'));

        that.hide();
        that.next().show().focus();
        that.next().blur(function () {
            saveChange($(this), varId);
        });

        that.next().keydown(event => {
            if (event.keyCode == 13) {
                saveChange($(this), varId);
            }
        });
    });

    function saveChange(input, varId) {
        const newText = input.val();

        if ($.trim(newText, varId)) {
            todoArr.forEach(value => {
                if (value._id == varId) {
                    newVar = value;
                    newVar.text = newText;
                    changeCheckTodo(newVar);
                }
            });


        }
    }

    function countTodo() {
            const   check_arr =_.where(todoArr, {checked: true}),
                    check = check_arr.length,
                    noCheck_arr =_.where(todoArr, {checked: false}),
                    noCheck = noCheck_arr.length,
                    allchecked = $("#allchecked");


        $("#noactivecheck").text(check);
        $("#activecheck").text(noCheck);
        if (check && check == allTodo) {
            allchecked.prop('checked', true);
        }
        else {
            allchecked.prop('checked', false);
        }
    }

    $('.menu-tab').click(function () {
        $('.menu-tab').removeClass('active');
        $(this).addClass('active');
        showTodo($(this).attr('id'));
    });

    function showTodo() {
        findNumber()
    }


    function findNumber(goLast) {
        const activeTab = $('.menu-tab.active').attr('id');
        let pagesNumber,
            activePage;

        if (activeTab == 'page-all') {
            allTodo = todoArr.length;
        }

        if (activeTab == 'page-active') {

            arr =_.where(todoArr, {checked: false});
            allTodo = arr.length;
        }

        else if (activeTab == 'page-completed') {
            arr =_.where(todoArr, {checked: true});
            allTodo = arr.length;
        }

        pagesNumber = Math.ceil(allTodo / liPerPage);
        activePage = (goLast) ? pagesNumber : Number($('.page_number.active').attr('id'));
        renderPages(pagesNumber, activePage);
    }


    function renderPages(pagesNumber, activePage) {
        let pagess='';
        if (!pagesNumber) {
            pagesNumber = 1;
        }
        $('#pages div').remove();


        for (let i = 1; i <= pagesNumber; i++) {

            let className = 'page_number';
            if (i == activePage) {
                className = className + ' active';
            }
            pagess += "<div class='" + className + "' id='" + i + "'>" + i + "</div>";
            $('#pages').html(pagess);
        }

        if ($('.page_number.active').length == 0) {
            $('.page_number:last-child').addClass('active');

        }

        showSlice(pagesNumber);
    }


    $("body").on("click", ".page_number", function () {
        $(".page_number").removeClass('active');
        $(this).addClass('active');
        showSlice($(this).attr('id'));
    });


    function showSlice(numpage) {
        const page = numpage || 1,
            a = page * 5,
            x = a - 5,
            activeTab = $('.menu-tab.active').attr('id');

        $('#todo-list li').remove();

        if (activeTab == 'page-all') {
            arr = todoArr;
            sliceArr (arr,x, a);
        }

        if (activeTab == 'page-active') {
            sliceArr (arr,x, a);
        }

        else if (activeTab == 'page-completed') {
            sliceArr (arr,x, a);
        }

    }
    function sliceArr (arr, x, a) {
        arrForPage = arr.slice(x, a);
        showPageTodo(arrForPage);
    }
    function showPageTodo(arrForPage){
        let ass= '';

        arrForPage.forEach(el => {
                let val = el.text;
            if (el.checked == true) {
                ass += '<li id="' + el._id + '"><input type="checkbox" class="checkbox" checked/><label class="hidetext checked" for=".checkbox">' + val + '</label><input class="text" style="display: none" value="' + val + '"><input type="submit" class="delete" value="Delete"/></li>';

            }
            else {
                ass += '<li id="' + el._id + '"><input type="checkbox" class="checkbox"/><label class="hidetext" for=".checkbox">' + val + '</label><input class="text" style="display: none" value="' + val + '"><input type="submit" class="delete" value="Delete"/></li>';

            }
        });

        todoList.html(ass);
        countTodo();
        showSpan();
    }


    function showSpan() {

        if (allTodo == 0) {
            ifNotTodo.show();
        }
        else {
            ifNotTodo.hide();
        }
    }
});

